// Package mocker easily mocks HTTP APIs and uses the well-known package
// https://pkg.go.dev/github.com/gorilla/mux for routing.
package mocker

import (
	"fmt"
	"net/http"
	"net/http/httptest"

	"github.com/gorilla/mux"
)

// APIMock is a generic HTTP API mock tool.
type APIMock struct {
	*httptest.Server
	*mux.Router
}

// Describe creates a new multiplexer for API mock by describing a set of
// HTTP route patterns associated with a http.Handler.
// See https://pkg.go.dev/github.com/gorilla/mux for examples.
func (mock *APIMock) Describe(descriptor func()) {
	mock.Router = mux.NewRouter()
	descriptor()
}

// NewAPIMock return a new API mock with a started local HTTP server.
func NewAPIMock() *APIMock {
	mock := &APIMock{
		Router: mux.NewRouter(),
	}
	mock.Server = httptest.NewServer(mock)
	return mock
}

// ResponseHandler is a handy http.Handler.
type ResponseHandler struct {
	code int
	body string
}

// NewResponseHandler instantiates a new HTTP response handler.
func NewResponseHandler(code int, body string) *ResponseHandler {
	return &ResponseHandler{code, body}
}
func (r *ResponseHandler) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(r.code)
	_, _ = w.Write([]byte(r.body))
}

// ScenarioHandler is a handy http.Handler which returns responses
// switch an ordered scenario.
type ScenarioHandler struct {
	roundNb  int
	scenario map[int]http.Handler
}

// NewScenarioHandler instantiates a new HTTP response handler.
func NewScenarioHandler(handlers map[int]http.Handler) *ScenarioHandler {
	return &ScenarioHandler{scenario: handlers}
}
func (sh *ScenarioHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	sh.roundNb++
	if scenario, found := sh.scenario[sh.roundNb]; !found {
		NewResponseHandler(
			http.StatusNotImplemented,
			fmt.Sprintf("scenario n°%d is not set for route %q", sh.roundNb, r.URL),
		).ServeHTTP(w, r)
	} else {
		scenario.ServeHTTP(w, r)
	}
}
