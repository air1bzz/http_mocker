# HTTP mocker

Package mocker easily mocks HTTP APIs and uses the well-known package
<https://pkg.go.dev/github.com/gorilla/mux> for routing.

```go
myMock := mocker.NewAPIMock()
myMock.Describe(func() {
    myMock.Path("/my_route").Handler(NewResponseHandler(http.StatusOK, "hello world !"))
    myMock.Path("/my_other_route").Handler(NewResponseHandler(http.StatusBadRequest, "goodbye world !"))
})
```
